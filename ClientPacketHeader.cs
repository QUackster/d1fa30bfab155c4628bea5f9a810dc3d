namespace Plus.Communication.Packets.Incoming
{
    public static class ClientPacketHeader
    {
        // Handshake
        public const int InitCryptoMessageEvent = 384;//1773
        public const int GenerateSecretKeyMessageEvent = 2408;//575
        public const int UniqueIDMessageEvent = 1865;//544
        public const int SSOTicketMessageEvent = 1266;//2093
        public const int InfoRetrieveMessageEvent = 3627;//413

        // Avatar
        public const int GetWardrobeMessageEvent = 220;//2913
        public const int SaveWardrobeOutfitMessageEvent = 2545;//3383

        // Catalog
        public const int GetCatalogIndexMessageEvent = 3029;//3626
        public const int GetCatalogPageMessageEvent = 3213;//3506
        public const int PurchaseFromCatalogMessageEvent = 3371;//2386
        public const int PurchaseFromCatalogAsGiftMessageEvent = 1420;//1467

        // Navigator

        // Messenger
        public const int GetBuddyRequestsMessageEvent = 3763;//2552

        // Quests
        public const int GetQuestListMessageEvent = 769;//2228
        public const int StartQuestMessageEvent = 2066;//3252
        public const int GetCurrentQuestMessageEvent = 1917;//3435
        public const int CancelQuestMessageEvent = 3842;//325

        // Room Avatar
        public const int ActionMessageEvent = 2448;//201
        public const int ApplySignMessageEvent = 2002;//1136
        public const int DanceMessageEvent = 2035;//2469
        public const int SitMessageEvent = 1531;//1752
        public const int ChangeMottoMessageEvent = 60;//2006
        public const int LookToMessageEvent = 1403;//376
        public const int DropHandItemMessageEvent = 1250;//2341

        // Room Connection
        public const int OpenFlatConnectionMessageEvent = 2503;//2437
        public const int GoToFlatMessageEvent = 2851;//2135

        // Room Chat
        public const int ChatMessageEvent = 2278;//4
        public const int ShoutMessageEvent = 3681;//3850
        public const int WhisperMessageEvent = 1039;//441

        // Room Engine

        // Room Furniture

        // Room Settings

        // Room Action

        // Users
        public const int GetIgnoredUsersMessageEvent = 3460;//1622

        // Moderation
        public const int OpenHelpToolMessageEvent = 3519;//1195
        public const int CallForHelpPendingCallsDeletedMessageEvent = 3174;//1104
        public const int ModeratorActionMessageEvent = 3544;//1063
        public const int ModerationMsgMessageEvent = 3138;//1511
        public const int ModerationMuteMessageEvent = 750;//2110
        public const int ModerationTradeLockMessageEvent = 1896;//22
        public const int GetModeratorUserRoomVisitsMessageEvent = 413;//1173
        public const int ModerationKickMessageEvent = 593;//110
        public const int GetModeratorRoomInfoMessageEvent = 413;//720
        public const int GetModeratorUserInfoMessageEvent = 2021;//3092
        public const int GetModeratorRoomChatlogMessageEvent = 35;//939
        public const int ModerateRoomMessageEvent = 1993;//3132
        public const int GetModeratorUserChatlogMessageEvent = 2026;//1876
        public const int GetModeratorTicketChatlogsMessageEvent = 3086;//2676
        public const int ModerationCautionMessageEvent = 1542;//3974
        public const int ModerationBanMessageEvent = 2915;//2065
        public const int SubmitNewTicketMessageEvent = 1467;//1842
        public const int CloseIssueDefaultActionEvent = 1143;//513

        // Inventory
        public const int GetCreditsInfoMessageEvent = 1583;//2919
        public const int GetAchievementsMessageEvent = 2618;//2069
        public const int GetBadgesMessageEvent = 2378;//2570
        public const int RequestFurniInventoryMessageEvent = 3674;//273
        public const int SetActivatedBadgesMessageEvent = 623;//3711
        public const int AvatarEffectActivatedMessageEvent = 3299;//3920
        public const int AvatarEffectSelectedMessageEvent = 3110;//771

        public const int InitTradeMessageEvent = 451;//2785
        public const int TradingCancelConfirmMessageEvent = 3054;//3276
        public const int TradingModifyMessageEvent = 1889;//3799
        public const int TradingOfferItemMessageEvent = 3978;//1814
        public const int TradingCancelMessageEvent = 1294;//1641
        public const int TradingConfirmMessageEvent = 2609;//937
        public const int TradingOfferItemsMessageEvent = 970;//1968
        public const int TradingRemoveItemMessageEvent = 2712;//3512
        public const int TradingAcceptMessageEvent = 2991;//347

        // Register
        public const int UpdateFigureDataMessageEvent = 2203;//920

        // Groups
        public const int GetBadgeEditorPartsMessageEvent = 497;//3879
        public const int GetGroupCreationWindowMessageEvent = 1180;//828
        public const int GetGroupFurniSettingsMessageEvent = 1804;//1590
        public const int DeclineGroupMembershipMessageEvent = 1289;//1154
        public const int JoinGroupMessageEvent = 2184;//1432
        public const int UpdateGroupColoursMessageEvent = 3444;//361
        public const int SetGroupFavouriteMessageEvent = 1178;//1384
        public const int GetGroupMembersMessageEvent = 667;//3441

        // Group Forums
        public const int PostGroupContentMessageEvent = 3545;//2126
        public const int GetForumStatsMessageEvent = 1027;//2812

        // Sound


        public const int RemoveMyRightsMessageEvent = 1363;//1962
        public const int GiveHandItemMessageEvent = 2078;//2378
        public const int GetClubGiftsMessageEvent = 507;//3949
        public const int GoToHotelViewMessageEvent = 2788;//3105
        public const int GetRoomFilterListMessageEvent = 1104;//3861
        public const int GetPromoArticlesMessageEvent = 3332;//2797
        public const int ModifyWhoCanRideHorseMessageEvent = 950;//961
        public const int RemoveBuddyMessageEvent = 3605;//996
        public const int RefreshCampaignMessageEvent = 2092;//1000
        public const int AcceptBuddyMessageEvent = 2606;//3194
        public const int YouTubeVideoInformationMessageEvent = 2587;//3568
        public const int FollowFriendMessageEvent = 2275;//436
        public const int SaveBotActionMessageEvent = 1230;//439
        public const int LetUserInMessageEvent = 3673;//1902
        public const int GetMarketplaceItemStatsMessageEvent = 1102;//2916
        public const int GetSellablePetBreedsMessageEvent = 841;//1278
        public const int ForceOpenCalendarBoxMessageEvent = 3115;//1319
        public const int SetFriendBarStateMessageEvent = 3142;//3756
        public const int DeleteRoomMessageEvent = 1698;//2298
        public const int SetSoundSettingsMessageEvent = 3894;//1199
        public const int InitializeGameCenterMessageEvent = 512;//744
        public const int RedeemOfferCreditsMessageEvent = 1776;//3290
        public const int FriendListUpdateMessageEvent = 2268;//3465
        public const int ConfirmLoveLockMessageEvent = 1869;//1439
        public const int UseHabboWheelMessageEvent = 1785;//1347
        public const int SaveRoomSettingsMessageEvent = 1134;//1294
        public const int ToggleMoodlightMessageEvent = 1793;//207
        public const int GetDailyQuestMessageEvent = 271;//3473
        public const int SetMannequinNameMessageEvent = 2415;//3687
        public const int UseOneWayGateMessageEvent = 3330;//3856
        public const int EventTrackerMessageEvent = 789;//2481
        public const int FloorPlanEditorRoomPropertiesMessageEvent = 3372;//2430
        public const int PickUpPetMessageEvent = 665;//3341
        public const int GetPetInventoryMessageEvent = 2599;//2500
        public const int InitializeFloorPlanSessionMessageEvent = 2536;//2722
        public const int GetOwnOffersMessageEvent = 401;//3671
        public const int CheckPetNameMessageEvent = 151;//230
        public const int SetUserFocusPreferenceEvent = 208;//3366
        public const int SubmitBullyReportMessageEvent = 1560;//3065
        public const int RemoveRightsMessageEvent = 1006;//3884
        public const int MakeOfferMessageEvent = 42;//2610
        public const int KickUserMessageEvent = 3149;//189
        public const int GetRoomSettingsMessageEvent = 1469;//1045
        public const int GetThreadsListDataMessageEvent = 2541;//193
        public const int GetForumUserProfileMessageEvent = 1218;//308
        public const int SaveWiredEffectConfigMessageEvent = 440;//2186
        public const int GetRoomEntryDataMessageEvent = 3235;//2815
        public const int JoinPlayerQueueMessageEvent = 2463;//1112
        public const int CanCreateRoomMessageEvent = 3150;//3007
        public const int SetTonerMessageEvent = 932;//244
        public const int SaveWiredTriggerConfigMessageEvent = 3248;//2454
        public const int PlaceBotMessageEvent = 2322;//215
        public const int GetRelationshipsMessageEvent = 2114;//1498
        public const int SetMessengerInviteStatusMessageEvent = 3335;//2742
        public const int UseFurnitureMessageEvent = 3272;//3069
        public const int GetUserFlatCatsMessageEvent = 1922;//1011
        public const int AssignRightsMessageEvent = 194;//661
        public const int GetRoomBannedUsersMessageEvent = 2818;//3678
        public const int ReleaseTicketMessageEvent = 3165;//1217
        public const int OpenPlayerProfileMessageEvent = 878;//2704
        public const int GetSanctionStatusMessageEvent = 1172;//3032
        public const int CreditFurniRedeemMessageEvent = 474;//1776
        public const int DisconnectionMessageEvent = 617;//1531
        public const int PickupObjectMessageEvent = 1539;//906
        public const int FindRandomFriendingRoomMessageEvent = 1607;//12
        public const int UseSellableClothingMessageEvent = 2804;//1441
        public const int MoveObjectMessageEvent = 3240;//3921
        public const int GetFurnitureAliasesMessageEvent = 3386;//3951
        public const int TakeAdminRightsMessageEvent = 1083;//3067
        public const int ModifyRoomFilterListMessageEvent = 2646;//2192
        public const int MoodlightUpdateMessageEvent = 3621;//2646
        public const int GetPetTrainingPanelMessageEvent = 1585;//146
        public const int GetSongInfoMessageEvent = 3007;//2081
        public const int UseWallItemMessageEvent = 2879;//1963
        public const int GetTalentTrackMessageEvent = 2215;//3795
        public const int GiveAdminRightsMessageEvent = 3876;//2415
        public const int GetCatalogModeMessageEvent = 1741;//2536
        public const int SendBullyReportMessageEvent = 2710;//3864
        public const int CancelOfferMessageEvent = 2738;//596
        public const int SaveWiredConditionConfigMessageEvent = 1071;//1399
        public const int RedeemVoucherMessageEvent = 1119;//64
        public const int ThrowDiceMessageEvent = 3308;//995
        public const int CraftSecretMessageEvent = 2086;//970
        public const int GetGameListingMessageEvent = 1324;//3736
        public const int SetRelationshipMessageEvent = 3500;//1577
        public const int RequestBuddyMessageEvent = 456;//3874
        public const int MemoryPerformanceMessageEvent = 3292;//457
        public const int ToggleYouTubeVideoMessageEvent = 2891;//1536
        public const int SetMannequinFigureMessageEvent = 504;//3555
        public const int GetEventCategoriesMessageEvent = 2679;//689
        public const int DeleteGroupThreadMessageEvent = 2154;//3166
        public const int PurchaseGroupMessageEvent = 882;//842
        public const int MessengerInitMessageEvent = 337;//190
        public const int CancelTypingMessageEvent = 1112;//483
        public const int GetMoodlightConfigMessageEvent = 3221;//3802
        public const int GetGroupInfoMessageEvent = 1015;//3880
        public const int CreateFlatMessageEvent = 2164;//186
        public const int LatencyTestMessageEvent = 501;//1483
        public const int GetSelectedBadgesMessageEvent = 527;//1169
        public const int AddStickyNoteMessageEvent = 3572;//1608
        public const int ChangeNameMessageEvent = 2350;//3822
        public const int RideHorseMessageEvent = 3863;//3162
        public const int InitializeNewNavigatorMessageEvent = 3808;//3583
        public const int SetChatPreferenceMessageEvent = 463;//2165
        public const int GetForumsListDataMessageEvent = 1637;//2984
        public const int ToggleMuteToolMessageEvent = 1573;//2891
        public const int UpdateGroupIdentityMessageEvent = 3283;//3977
        public const int UpdateStickyNoteMessageEvent = 2843;//2227
        public const int UnbanUserFromRoomMessageEvent = 1694;//223
        public const int UnIgnoreUserMessageEvent = 209;//3607
        public const int OpenGiftMessageEvent = 1821;//694
        public const int ApplyDecorationMessageEvent = 3767;//1841
        public const int GetRecipeConfigMessageEvent = 1680;//3060
        public const int ScrGetUserInfoMessageEvent = 661;//3305
        public const int RemoveGroupMemberMessageEvent = 828;//2143
        public const int DiceOffMessageEvent = 418;//2732
        public const int YouTubeGetNextVideo = 1232;//289
        public const int DeleteFavouriteRoomMessageEvent = 3502;//3438
        public const int RespectUserMessageEvent = 3136;//1394
        public const int AddFavouriteRoomMessageEvent = 1195;//1565
        public const int DeclineBuddyMessageEvent = 884;//1972
        public const int StartTypingMessageEvent = 563;//2422
        public const int GetGroupFurniConfigMessageEvent = 1810;//829
        public const int SendRoomInviteMessageEvent = 2992;//506
        public const int RemoveAllRightsMessageEvent = 163;//561
        public const int GetYouTubeTelevisionMessageEvent = 2957;//3517
        public const int FindNewFriendsMessageEvent = 712;//3216
        public const int GetPromotableRoomsMessageEvent = 605;//752
        public const int GetBotInventoryMessageEvent = 3244;//2952
        public const int GetRentableSpaceMessageEvent = 1517;//1572
        public const int OpenBotActionMessageEvent = 141;//844
        public const int OpenCalendarBoxMessageEvent = 3778;//2479
        public const int DeleteGroupPostMessageEvent = 41;//2377
        public const int CheckValidNameMessageEvent = 3487;//1666
        public const int UpdateGroupBadgeMessageEvent = 1707;//1984
        public const int PlaceObjectMessageEvent = 3993;//3061
        public const int RemoveGroupFavouriteMessageEvent = 46;//67
        public const int UpdateNavigatorSettingsMessageEvent = 910;//3629
        public const int CheckGnomeNameMessageEvent = 3116;//1055
        public const int NavigatorSearchMessageEvent = 2221;//3014
        public const int GetPetInformationMessageEvent = 2963;//950
        public const int GetGuestRoomMessageEvent = 3688;//2747
        public const int UpdateThreadMessageEvent = 547;//2964
        public const int AcceptGroupMembershipMessageEvent = 3582;//1280
        public const int GetMarketplaceConfigurationMessageEvent = 660;//3031
        public const int Game2GetWeeklyLeaderboardMessageEvent = 1784;//1452
        public const int BuyOfferMessageEvent = 49;//2583
        public const int RemoveSaddleFromHorseMessageEvent = 3699;//1843
        public const int GiveRoomScoreMessageEvent = 1307;//3585
        public const int GetHabboClubWindowMessageEvent = 1093;//1283
        public const int DeleteStickyNoteMessageEvent = 2563;//1603
        public const int MuteUserMessageEvent = 928;//2039
        public const int ApplyHorseEffectMessageEvent = 2409;//148
        public const int GetClientVersionMessageEvent = 4000;//4000
        public const int OnBullyClickMessageEvent = 185;//1048
        public const int HabboSearchMessageEvent = 329;//375
        public const int PickTicketMessageEvent = 1690;//1595
        public const int GetGiftWrappingConfigurationMessageEvent = 1913;//2200
        public const int GetCraftingRecipesAvailableMessageEvent = 3714;//1190
        public const int GetThreadDataMessageEvent = 3134;//30
        public const int ManageGroupMessageEvent = 201;//659
        public const int PlacePetMessageEvent = 1871;//967
        public const int EditRoomPromotionMessageEvent = 3821;//2297
        public const int GetCatalogOfferMessageEvent = 1786;//3483
        public const int SaveFloorPlanModelMessageEvent = 2872;//1230
        public const int MoveWallItemMessageEvent = 2101;//3208
        public const int ClientVariablesMessageEvent = 521;//647
        public const int PingMessageEvent = 1337;//3859
        public const int DeleteGroupMessageEvent = 1638;//991
        public const int UpdateGroupSettingsMessageEvent = 2323;//3420
        public const int GetRecyclerRewardsMessageEvent = 1471;//2854
        public const int PurchaseRoomPromotionMessageEvent = 3516;//1041
        public const int PickUpBotMessageEvent = 3841;//2997
        public const int GetOffersMessageEvent = 776;//566
        public const int GetHabboGroupBadgesMessageEvent = 2741;//298
        public const int GetUserTagsMessageEvent = 3893;//3593
        public const int GetPlayableGamesMessageEvent = 177;//2983
        public const int GetCatalogRoomPromotionMessageEvent = 2944;//3153
        public const int MoveAvatarMessageEvent = 3559;//719
        public const int SaveBrandingItemMessageEvent = 2211;//2210
        public const int SaveEnforcedCategorySettingsMessageEvent = 3865;//3486
        public const int RespectPetMessageEvent = 3845;//492
        public const int GetMarketplaceCanMakeOfferMessageEvent = 741;//3563
        public const int UpdateMagicTileMessageEvent = 3327;//1869
        public const int GetStickyNoteMessageEvent = 1594;//54
        public const int IgnoreUserMessageEvent = 2927;//556
        public const int BanUserMessageEvent = 3118;//614
        public const int UpdateForumSettingsMessageEvent = 2311;//2724
        public const int GetRoomRightsMessageEvent = 596;//902
        public const int SendMsgMessageEvent = 522;//677
        public const int CloseTicketMesageEvent = 1401;//3700
    }
}
