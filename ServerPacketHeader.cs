namespace Plus.Communication.Packets.Outgoing
{
    public static class ServerPacketHeader
    {
        // Handshake 
        public const int InitCryptoMessageComposer = 3182;//1114
        public const int SecretKeyMessageComposer = 2334;//2489
        public const int AuthenticationOKMessageComposer = 1523;//729
        public const int UserObjectMessageComposer = 3064;//167
        public const int UserPerksMessageComposer = 66;//2806
        public const int UserRightsMessageComposer = 1124;//3641
        public const int GenericErrorMessageComposer = 2106;//3532
        public const int SetUniqueIdMessageComposer = 1954;//651
        public const int AvailabilityStatusMessageComposer = 1939;//16

        // Avatar
        public const int WardrobeMessageComposer = 3123;//2466

        // Catalog
        public const int CatalogIndexMessageComposer = 3173;//2951
        public const int CatalogItemDiscountMessageComposer = 2352;//1920
        public const int PurchaseOKMessageComposer = 1861;//277
        public const int CatalogOfferMessageComposer = 3433;//1656
        public const int CatalogPageMessageComposer = 523;//634
        public const int CatalogUpdatedMessageComposer = 3579;//39
        public const int SellablePetBreedsMessageComposer = 2089;//1428
        public const int GroupFurniConfigMessageComposer = 2971;//2870
        public const int PresentDeliverErrorMessageComposer = 695;//1188

        // Quests
        public const int QuestListMessageComposer = 1208;//921
        public const int QuestCompletedMessageComposer = 628;//2565
        public const int QuestAbortedMessageComposer = 2794;//2103
        public const int QuestStartedMessageComposer = 943;//1895

        // Room Avatar
        public const int ActionMessageComposer = 3771;//2225
        public const int SleepMessageComposer = 3978;//526
        public const int DanceMessageComposer = 1911;//1969
        public const int CarryObjectMessageComposer = 3408;//371
        public const int AvatarEffectMessageComposer = 3639;//3063

        // Room Chat
        public const int ChatMessageComposer = 2899;//2628
        public const int ShoutMessageComposer = 1870;//1786
        public const int WhisperMessageComposer = 1338;//142
        public const int FloodControlMessageComposer = 1424;//1550
        public const int UserTypingMessageComposer = 2108;//2331

        // Room Engine
        public const int UsersMessageComposer = 3550;//3768
        public const int FurnitureAliasesMessageComposer = 1216;//90
        public const int ObjectAddMessageComposer = 2873;//2682
        public const int ObjectsMessageComposer = 1559;//370
        public const int ObjectUpdateMessageComposer = 3621;//1590
        public const int ObjectRemoveMessageComposer = 2511;//1552
        public const int SlideObjectBundleMessageComposer = 2112;//66
        public const int ItemsMessageComposer = 2400;//661
        public const int ItemAddMessageComposer = 3779;//2163
        public const int ItemUpdateMessageComposer = 3634;//343
        public const int ItemRemoveMessageComposer = 3667;//900

        // Room Session
        public const int RoomForwardMessageComposer = 1823;//3695
        public const int RoomReadyMessageComposer = 3657;//3964
        public const int OpenConnectionMessageComposer = 761;//1336
        public const int CloseConnectionMessageComposer = 2364;//3035
        public const int FlatAccessibleMessageComposer = 650;//2629
        public const int CantConnectMessageComposer = 3264;//1561

        // Room Permissions
        public const int YouAreControllerMessageComposer = 2627;//1627
        public const int YouAreNotControllerMessageComposer = 1613;//22
        public const int YouAreOwnerMessageComposer = 2683;//911

        // Room Settings
        public const int RoomSettingsDataMessageComposer = 2897;//2338
        public const int RoomSettingsSavedMessageComposer = 2075;//562
        public const int FlatControllerRemovedMessageComposer = 2152;//3668
        public const int FlatControllerAddedMessageComposer = 2667;//199
        public const int RoomRightsListMessageComposer = 947;//2635

        // Room Furniture
        public const int HideWiredConfigMessageComposer = 3508;//1469
        public const int WiredEffectConfigMessageComposer = 720;//161
        public const int WiredConditionConfigMessageComposer = 929;//3401
        public const int WiredTriggerConfigMessageComposer = 1670;//1442
        public const int MoodlightConfigMessageComposer = 508;//1753
        public const int GroupFurniSettingsMessageComposer = 3167;//298
        public const int OpenGiftMessageComposer = 2058;//899

        // Navigator
        public const int UpdateFavouriteRoomMessageComposer = 3189;//1433
        public const int NavigatorLiftedRoomsMessageComposer = 3958;//2199
        public const int NavigatorPreferencesMessageComposer = 1107;//2440
        public const int NavigatorFlatCatsMessageComposer = 2228;//1163
        public const int NavigatorMetaDataParserMessageComposer = 1855;//3309
        public const int NavigatorCollapsedCategoriesMessageComposer = 1695;//364

        // Messenger
        public const int BuddyListMessageComposer = 1053;//1475
        public const int BuddyRequestsMessageComposer = 1397;//614
        public const int NewBuddyRequestMessageComposer = 154;//1327

        // Moderation
        public const int ModeratorInitMessageComposer = 3743;//2354
        public const int ModeratorUserRoomVisitsMessageComposer = 2114;//2413
        public const int ModeratorRoomChatlogMessageComposer = 2393;//1568
        public const int ModeratorUserInfoMessageComposer = 3047;//1827
        public const int ModeratorSupportTicketResponseMessageComposer = 1922;//3488
        public const int ModeratorUserChatlogMessageComposer = 1816;//981
        public const int ModeratorRoomInfoMessageComposer = 3251;//1103
        public const int ModeratorSupportTicketMessageComposer = 2462;//18
        public const int ModeratorTicketChatlogMessageComposer = 2181;//2224
        public const int CallForHelpPendingCallsMessageComposer = 1588;//3427
        public const int CfhTopicsInitMessageComposer = 1386;//862

        // Inventory
        public const int CreditBalanceMessageComposer = 3204;//1860
        public const int BadgesMessageComposer = 1919;//556
        public const int FurniListAddMessageComposer = 3861;//175
        public const int FurniListNotificationMessageComposer = 116;//697
        public const int FurniListRemoveMessageComposer = 2096;//2509
        public const int FurniListMessageComposer = 1356;//2165
        public const int FurniListUpdateMessageComposer = 165;//3789
        public const int AvatarEffectsMessageComposer = 3868;//3994
        public const int AvatarEffectActivatedMessageComposer = 546;//2292
        public const int AvatarEffectExpiredMessageComposer = 3417;//1676
        public const int AvatarEffectAddedMessageComposer = 3123;//2466
        public const int TradingErrorMessageComposer = 825;//1921
        public const int TradingAcceptMessageComposer = 446;//1421
        public const int TradingStartMessageComposer = 1033;//347
        public const int TradingUpdateMessageComposer = 3760;//3123
        public const int TradingClosedMessageComposer = 1542;//2822
        public const int TradingCompleteMessageComposer = 655;//1728
        public const int TradingConfirmedMessageComposer = 446;//1421
        public const int TradingFinishMessageComposer = 406;//3107

        // Inventory Achievements
        public const int AchievementsMessageComposer = 403;//2950
        public const int AchievementScoreMessageComposer = 617;//687
        public const int AchievementUnlockedMessageComposer = 1435;//3458
        public const int AchievementProgressedMessageComposer = 3027;//47

        // Notifications
        public const int ActivityPointsMessageComposer = 3289;//1902
        public const int HabboActivityPointNotificationMessageComposer = 2802;//3182

        // Users
        public const int ScrSendUserInfoMessageComposer = 3730;//223
        public const int IgnoredUsersMessageComposer = 172;//636

        // Groups
        public const int UnknownGroupMessageComposer = 1977;//2710
        public const int GroupMembershipRequestedMessageComposer = 3313;//1889
        public const int ManageGroupMessageComposer = 3438;//126
        public const int HabboGroupBadgesMessageComposer = 2165;//3331
        public const int NewGroupInfoMessageComposer = 989;//3587
        public const int GroupInfoMessageComposer = 1110;//595
        public const int GroupCreationWindowMessageComposer = 3155;//2555
        public const int SetGroupIdMessageComposer = 2672;//3941
        public const int GroupMembersMessageComposer = 85;//3694
        public const int UpdateFavouriteGroupMessageComposer = 49;//160
        public const int GroupMemberUpdatedMessageComposer = 2774;//234
        public const int RefreshFavouriteGroupMessageComposer = 777;//653

        // Group Forums
        public const int ForumsListDataMessageComposer = 3582;//3470
        public const int ForumDataMessageComposer = 3962;//742
        public const int ThreadCreatedMessageComposer = 3698;//28
        public const int ThreadDataMessageComposer = 1522;//1995
        public const int ThreadsListDataMessageComposer = 1246;//698
        public const int ThreadUpdatedMessageComposer = 263;//3938
        public const int ThreadReplyMessageComposer = 1450;//140

        // Sound
        public const int SoundSettingsMessageComposer = 2317;//3579

        public const int QuestionParserMessageComposer = 3174;//1423
        public const int AvatarAspectUpdateMessageComposer = 1412;//750
        public const int HelperToolMessageComposer = 1357;//2900
        public const int RoomErrorNotifMessageComposer = 2998;//960
        public const int FollowFriendFailedMessageComposer = 3154;//1951

        public const int FindFriendsProcessResultMessageComposer = 1877;//1861
        public const int UserChangeMessageComposer = 1126;//1460
        public const int FloorHeightMapMessageComposer = 3091;//813
        public const int RoomInfoUpdatedMessageComposer = 1626;//2740
        public const int MessengerErrorMessageComposer = 3644;//3678
        public const int MarketplaceCanMakeOfferResultMessageComposer = 1232;//704
        public const int GameAccountStatusMessageComposer = 3833;//1703
        public const int GuestRoomSearchResultMessageComposer = 2752;//3953
        public const int NewUserExperienceGiftOfferMessageComposer = 2180;//1259
        public const int UpdateUsernameMessageComposer = 72;//3700
        public const int VoucherRedeemOkMessageComposer = 2151;//1736
        public const int FigureSetIdsMessageComposer = 3812;//3772
        public const int StickyNoteMessageComposer = 2293;//377
        public const int UserRemoveMessageComposer = 1506;//2381
        public const int GetGuestRoomResultMessageComposer = 2172;//3018
        public const int DoorbellMessageComposer = 2541;//1905

        public const int GiftWrappingConfigurationMessageComposer = 938;//3388
        public const int GetRelationshipsMessageComposer = 3926;//2887
        public const int FriendNotificationMessageComposer = 1800;//2441
        public const int BadgeEditorPartsMessageComposer = 627;//509
        public const int TraxSongInfoMessageComposer = 295;//2016
        public const int PostUpdatedMessageComposer = 3929;//831
        public const int UserUpdateMessageComposer = 1434;//477
        public const int MutedMessageComposer = 1582;//739
        public const int MarketplaceConfigurationMessageComposer = 2539;//228
        public const int CheckGnomeNameMessageComposer = 25;//2858
        public const int OpenBotActionMessageComposer = 3267;//350
        public const int FavouritesMessageComposer = 908;//1515
        public const int TalentLevelUpMessageComposer = 1617;//2416

        public const int BCBorrowedItemsMessageComposer = 3400;//1937
        public const int UserTagsMessageComposer = 2713;//236
        public const int CampaignMessageComposer = 3763;//3113
        public const int RoomEventMessageComposer = 1000;//1680
        public const int MarketplaceItemStatsMessageComposer = 1436;//2796
        public const int HabboSearchResultMessageComposer = 1118;//2448
        public const int PetHorseFigureInformationMessageComposer = 1250;//712
        public const int PetInventoryMessageComposer = 3232;//3005
        public const int PongMessageComposer = 824;//2947
        public const int RentableSpaceMessageComposer = 1078;//1224
        public const int GetYouTubePlaylistMessageComposer = 1995;//720
        public const int RespectNotificationMessageComposer = 1004;//1348
        public const int RecyclerRewardsMessageComposer = 1395;//2935
        public const int GetRoomBannedUsersMessageComposer = 2509;//1084
        public const int RoomRatingMessageComposer = 3339;//3422
        public const int PlayableGamesMessageComposer = 2934;//345
        public const int TalentTrackLevelMessageComposer = 3258;//579
        public const int JoinQueueMessageComposer = 2347;//2241
        public const int MarketPlaceOwnOffersMessageComposer = 1610;//1105
        public const int PetBreedingMessageComposer = 3270;//3884
        public const int SubmitBullyReportMessageComposer = 236;//3344
        public const int UserNameChangeMessageComposer = 3170;//405
        public const int LoveLockDialogueMessageComposer = 3172;//2504
        public const int SendBullyReportMessageComposer = 2316;//574
        public const int VoucherRedeemErrorMessageComposer = 3798;//3741
        public const int PurchaseErrorMessageComposer = 3392;//2030
        public const int UnknownCalendarMessageComposer = 2059;//779
        public const int FriendListUpdateMessageComposer = 988;//2561

        public const int UserFlatCatsMessageComposer = 264;//3974
        public const int UpdateFreezeLivesMessageComposer = 371;//1651
        public const int UnbanUserFromRoomMessageComposer = 1385;//3020
        public const int PetTrainingPanelMessageComposer = 412;//3330
        public const int LoveLockDialogueCloseMessageComposer = 451;//3244
        public const int BuildersClubMembershipMessageComposer = 1517;//383
        public const int FlatAccessDeniedMessageComposer = 3016;//1042
        public const int LatencyResponseMessageComposer = 2213;//1603
        public const int HabboUserBadgesMessageComposer = 574;//3545
        public const int HeightMapMessageComposer = 3602;//3893

        public const int CanCreateRoomMessageComposer = 434;//1996
        public const int InstantMessageErrorMessageComposer = 1822;//3880
        public const int GnomeBoxMessageComposer = 1631;//2867
        public const int IgnoreStatusMessageComposer = 1744;//2607
        public const int PetInformationMessageComposer = 3915;//2582
        public const int NavigatorSearchResultSetMessageComposer = 1598;//2096
        public const int ConcurrentUsersGoalProgressMessageComposer = 348;//500
        public const int VideoOffersRewardsMessageComposer = 813;//474
        public const int SanctionStatusMessageComposer = 2650;//1991
        public const int GetYouTubeVideoMessageComposer = 875;//972
        public const int CheckPetNameMessageComposer = 480;//3196
        public const int RespectPetNotificationMessageComposer = 3364;//2497
        public const int EnforceCategoryUpdateMessageComposer = 1876;//2109
        public const int CommunityGoalHallOfFameMessageComposer = 957;//1035
        public const int FloorPlanFloorMapMessageComposer = 996;//2599
        public const int SendGameInvitationMessageComposer = 383;//1429
        public const int GiftWrappingErrorMessageComposer = 2985;//799
        public const int PromoArticlesMessageComposer = 2512;//1837
        public const int Game1WeeklyLeaderboardMessageComposer = 2820;//75
        public const int RentableSpacesErrorMessageComposer = 71;//761
        public const int AddExperiencePointsMessageComposer = 1041;//611
        public const int OpenHelpToolMessageComposer = 1588;//3427
        public const int GetRoomFilterListMessageComposer = 1888;//2011
        public const int GameAchievementListMessageComposer = 1828;//74
        public const int PromotableRoomsMessageComposer = 2706;//947
        public const int FloorPlanSendDoorMessageComposer = 621;//3586
        public const int RoomEntryInfoMessageComposer = 2620;//200
        public const int RoomNotificationMessageComposer = 1178;//1842
        public const int ClubGiftsMessageComposer = 3399;//2069
        public const int MOTDNotificationMessageComposer = 3551;//748
        public const int PopularRoomTagsResultMessageComposer = 1054;//1422
        public const int NewConsoleMessageMessageComposer = 260;//1335
        public const int RoomPropertyMessageComposer = 1154;//2274
        public const int MarketPlaceOffersMessageComposer = 1719;//550
        public const int TalentTrackMessageComposer = 118;//210
        public const int ProfileInformationMessageComposer = 1455;//3514
        public const int BadgeDefinitionsMessageComposer = 1173;//1851
        public const int Game2WeeklyLeaderboardMessageComposer = 1983;//3247
        public const int NameChangeUpdateMessageComposer = 3338;//2687
        public const int RoomVisualizationSettingsMessageComposer = 45;//1560
        public const int MarketplaceMakeOfferResultMessageComposer = 1436;//2796
        public const int FlatCreatedMessageComposer = 2479;//1299
        public const int BotInventoryMessageComposer = 185;//1602
        public const int LoadGameMessageComposer = 1605;//904
        public const int UpdateMagicTileMessageComposer = 3255;//3523
        public const int CampaignCalendarDataMessageComposer = 680;//2859
        public const int MaintenanceStatusMessageComposer = 447;//2291
        public const int Game3WeeklyLeaderboardMessageComposer = 3628;//2943
        public const int GameListMessageComposer = 1831;//823
        public const int RoomMuteSettingsMessageComposer = 1493;//1790
        public const int RoomInviteMessageComposer = 3342;//3436
        public const int LoveLockDialogueSetLockedMessageComposer = 451;//3244
        public const int BroadcastMessageAlertMessageComposer = 1367;//3566
        public const int MarketplaceCancelOfferResultMessageComposer = 1610;//1105
        public const int NavigatorSettingsMessageComposer = 456;//3255

        public const int MessengerInitMessageComposer = 2496;//1984
    }
}
